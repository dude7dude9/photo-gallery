'use strict';

//import Gallery from './gallery';

window.onload = () => {
	var galleryApp = {};
	
	galleryApp.galleryComponent = ReactDOM.render(<Gallery/>, document.querySelector('#gallery'));
	galleryApp.galleryComponent.loadImages();
}

// Testing setup
// window.onload = (event)=>{
	// //var gallery = new Gallery();
	
	// const domContainer = document.querySelector('#root');
	// //ReactDOM.render(e(gallery), domContainer);
	// // ReactDOM.render(gallery.render(), domContainer);
	
	// // Have to use weird HTML syntax to initialze React.Component & mount it
	// // ReactDOM.render(<Gallery/>, domContainer);
	// document.bla = React.createElement(Gallery);
	// document.foo = <Gallery/>; // This is JSX equivalent to above
	
	// // This is how to get instance of a component so that you can invoke instance methods later if desired
	// // Not sure if you can access child components through this, probably would need to store child nodes as a prop
	// document.bar = ReactDOM.render(document.bla, domContainer);
// }