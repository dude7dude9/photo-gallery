class Gallery extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = { images: [] };
  }

  loadImages(){
	  this.setState({
		images: [
			'https://images.unsplash.com/photo-1476610182048-b716b8518aae?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
			'https://images.unsplash.com/photo-1504714146340-959ca07e1f38?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
			'https://images.unsplash.com/photo-1500322969630-a26ab6eb64cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
			'https://image.freepik.com/free-photo/blue-mountains-famous-tourism-scenery-lijiang_1417-1143.jpg']
		  });
  }
  
  componentDidMount(){}
  
  componentWillUnmount(){}
  
  
  render() {
	return (
		<ul>
			{this.state.images.map((image) => 
				<li key={image.toString()}><img src={image}/></li>
			)}
		</ul>
	);
  }
}