//const createEle = React.createElement;

class Photo extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = { liked: false };
  }

  render() {
    if (this.state.liked) {
      return 'You liked this Too?';
    }

    return createEle(
      'button',
      { onClick: () => this.setState({ liked: true }) },
      'Like'
    );
  }
  
}