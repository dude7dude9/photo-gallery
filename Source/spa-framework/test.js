const createEle = React.createElement;
console.log('load Gallery');

// export default
class Gallery extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = { liked: false };
  }

  clickTest(e){
    console.log('clicked');
	this.setState({liked:true});
  }
  
  componentWillUnmount(){
	  console.log('Will unmount');
	  console.log('Will unMount at: ' + Date.now());
  }
  
  componentDidMount(){
	  console.log('Did mount');
	  console.log('Did Mount at: ' + Date.now());
  }
  
  render() {
	console.log('render');
	console.log(this.state);
	  
    if (this.state.liked) {
      return (<p>You liked</p>);
    }

	// return(<p>Not Liked</p>);
	return (
		<button onClick={()=> this.clickTest(this)}>
			I Like!
		</button>);
    // return createEle(
      // 'button',
      // { onClick: () => this.setState({ liked: true }) },
      // 'Like'
    // );
  }
  
  //ReactDOM.render(e(LikeButton), domContainer);
}