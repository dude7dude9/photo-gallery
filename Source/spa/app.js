'use strict';

window.onload = () => {
    var urlPath = window.location.pathname;
    var urlParts = urlPath.split('/');
    var id = urlParts.length ? urlParts[1].split('id:')[1] : '';
    history.pushState(id, null, urlPath);

    Routing.loadAppPage(urlPath);

    // Run internal links through history API
    $(document).on('click', 'a', (e) => {
        if (e.target != e.currentTarget) {
            e.preventDefault();

            var urlPath = e.currentTarget.getAttribute('href');
            var id = urlPath.split('id:')[1];
            history.pushState(id, null, urlPath);
            Routing.loadAppPage(urlPath);
        }

        e.stopPropagation();
    });

    // Fired on forward/back browser actions to load page history without refresh needed
    window.addEventListener('popstate', (e) => {
        var urlPath = window.location.pathname;
        Routing.loadAppPage(urlPath);

        if (urlPath.split('/').length > 1) {
            Gallery.highlightViewedItem(sessionStorage.getItem('viewedItem'));
        }
    });
}