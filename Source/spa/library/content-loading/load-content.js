// Content Loader designed to allow integration with a range of different datasources
// To add or remove datasources simply pass a function to the constructor or 'AddContentLoader' 
// which is then used when loading content
class LoadContent {
    constructor(contentLoaders) {
        this.contentLoaders = contentLoaders || [];
    }

    addContentLoader(contentLoaders) {
        this.contentLoaders.push(contentLoaders);

        return this;
    }

    loadContent() {
        var content = [];

        // Content loaders protected by try catch so that if external service goes down
        // the gallery continues running regardless of what happened, to the extent that it will work until
        // all datasources are down
        this.contentLoaders.forEach((contentLoader) => {
            try {
                // Add content with a valid base type
                var tempItems = contentLoader().filter((contentItem) =>
                    contentItem instanceof BaseContent);

                content = content.concat(tempItems);
            }
            catch (error) {
                // Error not thrown so that integrated services don't take down app
                console.log(error); 
                //TODO: log errors on server side - if had additional time then notifying this would be useful
            }
        });

        return content;
    }
}