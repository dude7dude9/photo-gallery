// This class is a mock to simulate getting files from an external data source.
// LoadContent is when getting all files for the gallery view
// LoadFile is when searching for a specific file used in the item viewer view
class StaticPictureLoader {
    static loadFile(id) {
        var file;

        switch (id) {
            case 'GUID-1':
                file = this.File1;
                break;
            case 'GUID-2':
                file = this.File2;
                break;
            case 'GUID-3':
                file = this.File3;
                break;
            case 'GUID-4':
                file = this.File4;
                break;
            case 'GUID-5':
                file = this.File5;
                break;
            case 'GUID-6':
                file = this.File6;
                break;
            case 'GUID-7':
                file = this.File7;
                break;
            case 'GUID-8':
                file = this.File8;
                break;
            case 'GUID-9':
                file = this.File9;
                break;
            case 'GUID-10':
                file = this.File10;
                break;
            case 'GUID-11':
                file = this.File11;
                break;
            case 'GUID-12':
                file = this.File12;
                break;
            case 'GUID-13':
                file = this.File13;
                break;
            case 'GUID-14':
                file = this.File14;
                break;
            default:
                throw new Error('File Unknown');
        }

        return [file];
    }

    static loadContent() {
        // Fake pull from DB with DB item and Datatypes
        // Fake parse into correct datatype

        return [
            this.File1,
            this.File2,
            this.File3,
            this.File4,
            this.File5,
            this.File6,
            this.File7,
            this.File8,
            this.File9,
            this.File10,
            this.File11,
            this.File12,
            this.File13,
            this.File14
        ];
    }


    static get File1() {
        return new Picture({
            createdDate: '',
            createdBy: '',
            lastModifiedDate: '',
            lastModifiedBy: '',
            name: 'Raining City',
            description: 'Raining City view',
            id: 'GUID-1',
            fileSize: '50',
            type: 'JPG',
            url: 'https://images.pexels.com/photos/531880/pexels-photo-531880.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'
        });
    }

    static get File2() {
        return new Picture({
            createdDate: '',
            createdBy: '',
            lastModifiedDate: '',
            lastModifiedBy: '',
            name: 'Purple Fields',
            description: 'Purple Fields',
            id: 'GUID-2',
            fileSize: '50',
            type: 'JPG',
            url: 'https://www.bhmpics.com/thumbs/sunset_flower_scenery-t3.jpg'
        });
    }

    static get File3() {
        return new Picture({
            createdDate: '',
            createdBy: '',
            lastModifiedDate: '',
            lastModifiedBy: '',
            name: 'Smoke',
            description: 'Smoky Room',
            id: 'GUID-3',
            fileSize: '50',
            type: 'JPG',
            url: 'https://media.istockphoto.com/photos/texture-dark-concrete-floor-picture-id621925576?k=6&m=621925576&s=612x612&w=0&h=H4rph08J2vSJz_crupTLvHf5OKW0olvoRJzaAK37Yhg='
        });
    }

    static get File4() {
        return new Picture({
            createdDate: '',
            createdBy: '',
            lastModifiedDate: '',
            lastModifiedBy: '',
            name: 'Colored Smoke',
            description: 'Colored Smoke',
            id: 'GUID-4',
            fileSize: '50',
            type: 'JPG',
            url: 'https://images.pexels.com/photos/1020315/pexels-photo-1020315.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'
        });
    }

    static get File5() {
        return new Picture({
            createdDate: '',
            createdBy: '',
            lastModifiedDate: '',
            lastModifiedBy: '',
            name: 'Garden Light',
            description: 'Light shining through the trees',
            id: 'GUID-5',
            fileSize: '50',
            type: 'JPG',
            url: 'https://image.freepik.com/free-photo/blurred-park-with-bokeh-light-nature-background_7190-761.jpg'
        });
    }

    static get File6() {
        return new Picture({
            createdDate: '',
            createdBy: '',
            lastModifiedDate: '',
            lastModifiedBy: '',
            name: 'Flowers',
            description: 'Flowers view',
            id: 'GUID-6',
            fileSize: '50',
            type: 'JPG',
            url: 'https://esal.info/wp-content/uploads/2018/08/rose-bring-soil-hands-flower-scenery-wallpapers-awesome-84-best-wallpaper-images-on-pinterest-of-rose-bring-soil-hands-flower-scenery-wallpapers-1.jpg'
        });
    }

    static get File7() {
        return new Picture({
            createdDate: '',
            createdBy: '',
            lastModifiedDate: '',
            lastModifiedBy: '',
            name: 'Bricks',
            description: 'Brick Wall',
            id: 'GUID-7',
            fileSize: '50',
            type: 'JPG',
            url: 'https://images.pexels.com/photos/1308624/pexels-photo-1308624.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'
        });
    }

    static get File8() {
        return new Picture({
            createdDate: '',
            createdBy: '',
            lastModifiedDate: '',
            lastModifiedBy: '',
            name: 'Raining City 2',
            description: 'Raining City view',
            id: 'GUID-8',
            fileSize: '50',
            type: 'JPG',
            url: 'https://images.pexels.com/photos/531880/pexels-photo-531880.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'
        });
    }

    static get File9() {
        return new Picture({
            createdDate: '',
            createdBy: '',
            lastModifiedDate: '',
            lastModifiedBy: '',
            name: 'Purple Fields 2',
            description: 'Purple fields',
            id: 'GUID-9',
            fileSize: '50',
            type: 'JPG',
            url: 'https://www.bhmpics.com/thumbs/sunset_flower_scenery-t3.jpg'
        });
    }

    static get File10() {
        return new Picture({
            createdDate: '',
            createdBy: '',
            lastModifiedDate: '',
            lastModifiedBy: '',
            name: 'Smoke 2',
            description: 'Smoky Room',
            id: 'GUID-10',
            fileSize: '50',
            type: 'JPG',
            url: 'https://media.istockphoto.com/photos/texture-dark-concrete-floor-picture-id621925576?k=6&m=621925576&s=612x612&w=0&h=H4rph08J2vSJz_crupTLvHf5OKW0olvoRJzaAK37Yhg='
        });
    }

    static get File11() {
        return new Picture({
            createdDate: '',
            createdBy: '',
            lastModifiedDate: '',
            lastModifiedBy: '',
            name: 'Colored Smoke 2',
            description: 'Colored Smoke',
            id: 'GUID-11',
            fileSize: '50',
            type: 'JPG',
            url: 'https://images.pexels.com/photos/1020315/pexels-photo-1020315.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'
        });
    }

    static get File12() {
        return new Picture({
            createdDate: '',
            createdBy: '',
            lastModifiedDate: '',
            lastModifiedBy: '',
            name: 'Garden Light 2',
            description: 'Light shining through the trees',
            id: 'GUID-12',
            fileSize: '50',
            type: 'JPG',
            url: 'https://image.freepik.com/free-photo/blurred-park-with-bokeh-light-nature-background_7190-761.jpg'
        });
    }

    static get File13() {
        return new Picture({
            createdDate: '',
            createdBy: '',
            lastModifiedDate: '',
            lastModifiedBy: '',
            name: 'Flowers 2',
            description: 'Flowers view',
            id: 'GUID-13',
            fileSize: '50',
            type: 'JPG',
            url: 'https://esal.info/wp-content/uploads/2018/08/rose-bring-soil-hands-flower-scenery-wallpapers-awesome-84-best-wallpaper-images-on-pinterest-of-rose-bring-soil-hands-flower-scenery-wallpapers-1.jpg'
        });
    }

    static get File14() {
        return new Picture({
            createdDate: '',
            createdBy: '',
            lastModifiedDate: '',
            lastModifiedBy: '',
            name: 'Bricks 2',
            description: 'Brick Wall',
            id: 'GUID-14',
            fileSize: '50',
            type: 'JPG',
            url: 'https://images.pexels.com/photos/1308624/pexels-photo-1308624.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'
        });
    }

}