// Content Type Interface
// Defines the standard criteria for gallery items
// This allows filtering, sorting, searching etc
class BaseContent {

    constructor(contentObj) {
        this.createdDate = contentObj.createdDate;
        this.createdBy = contentObj.createdBy;
        this.lastModifiedDate = contentObj.lastModifiedDate;
        this.lastModifiedBy = contentObj.lastModifiedBy;
        this.name = contentObj.name;
        this.description = contentObj.description;
        this.id = contentObj.id;
        this.fileSize = contentObj.fileSize;
        this.type = contentObj.type;
        this.url = contentObj.url;

        // Ensure that the required field members exist on all content types
        if (!this.name || !this.id || !this.url) {
            throw new Error('Content Type missing required arguments for initialization');
        }
    }

    // Ensure that interface methods are overridden
    renderGallery() {
        throw new Error('Content Rendering method not implemented');
    }

    renderViewer() {
        throw new Error('Content Rendering method not implemented');
    }

    get galleryTemplate() {
        throw new Error('Content Template not defined');
    }

    get viewerTemplate() {
        throw new Error('Content Template not defined');
    }
}