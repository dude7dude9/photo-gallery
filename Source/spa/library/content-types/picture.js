class Picture extends BaseContent {
    constructor(contentObj) {
        super(contentObj);

        this.image = new Image();
    }

    renderViewer() {
        this.image.src = this.url;
        var node = Mustache.render(
            this.viewerTemplate, this);

        return node;
    }

    renderGallery() {
        var node = Mustache.render(
            this.galleryTemplate, this);

        return node;
    }

    get nextItem() {

        var idParts = this.id.split('-');
        var idNum = parseInt(idParts[1]) + 1;

        if (idNum < 15) {
            var nextItem = StaticPictureLoader.loadFile(idParts[0] + '-' + idNum)[0];

            return Mustache.render('<div data-id="{{id}}" class="next content hide"><img src="{{url}}" alt="{{description}}"/></div>', nextItem);
        }

        return '';
    }

    get previousItem() {
        var idParts = this.id.split('-');
        var idNum = parseInt(idParts[1]) -1;

        if (idNum > 0) {
            var nextItem = StaticPictureLoader.loadFile(idParts[0] + '-' + idNum)[0];

            return Mustache.render('<div data-id="{{id}}" class="previous content hide"><img src="{{url}}" alt="{{description}}"/></div>', nextItem);
        }

        return '';
    }

    get galleryTemplate() {
        var template =
            '<li class="item {{id}}">' +
                '<a href="/id:{{id}}">' +
                    '<img src="{{url}}" alt="{{description}}"/>' +
                '</a>' +
            '</li>';

        return template;
    }

    get viewerTemplate() {
        var template =
            '<header class="app-header">' +
                '<h1 class="title">{{name}}</h1>' +
                '<button class="close"><span class="fas fa-times"></span></button>' +
            '</header>' +
            '<main class="screen-width-limit slider">' +
                '<div class="swipe-wrap">' +
                    this.previousItem +
                    '<div data-id="{{id}}" class="content current show">' +
                        '<img src="{{url}}" alt="{{description}}"/>' +
                    '</div>' +
                    this.nextItem +
                '</div>' +
            '</main>';

        return template;
    }
}