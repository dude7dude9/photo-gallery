class Video extends BaseContent {
    constructor(contentObj) {
        super(contentObj);
    }

    renderViewer() {
        var node = Mustache.render(
            this.viewerTemplate, this);

        return node;
    }

    renderGallery() {
        var node = Mustache.render(
            this.galleryTemplate, this);

        return node;
    }

    get galleryTemplate() {
        var template =
            '<li class="item">' +
                '<a href="/id:{{id}}">' +
                    '<img src="{{url}}" alt="{{description}}"/>' +
                '</a>' +
            '</li>';

        return template;
    }

    get viewerTemplate() {
        var template =
            '<div class="content">' +
                '<img src="{{url}}" alt="{{description}}"/>' +
            '</div>';

        return template;
    }
}