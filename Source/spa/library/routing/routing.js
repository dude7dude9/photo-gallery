// Site SPA routing - on larger project this would instead be framework routing via React or Angular
class Routing {
    static loadAppPage(urlPath) {
        // Ensure dynamically bound events aren't being duplicated 
        $('body').off('click');

        switch (true) {
            // Root URL / 
            // The listing gallery page
            case /^\/$/.test(urlPath):  
                $('body').removeClass('page-viewer');
                $('body').addClass('page-gallery');

                var gallery = new Gallery('body');
                gallery.loadFiles();
                gallery.render();
                break;

            // View Item URL /id:{GUID}
            // The item viewer page
            case /^\/id:[^\/]+$/.test(urlPath):  
                $('body').removeClass('page-gallery');
                $('body').addClass('page-viewer');

                var urlParts = urlPath.split('/')[1];
                var id = urlParts.split('id:')[1];

                var viewer = new Viewer('body');
                viewer.loadFile(id);
                viewer.render();
                break;
        }
    }
}