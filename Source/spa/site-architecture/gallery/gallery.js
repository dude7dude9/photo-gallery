class Gallery {

    constructor(elementSelector) {
        this.content = []; // Images, videos, posts etc
        this.elementSelector = elementSelector;
    }

    loadFiles() {
        var contentLoader = new LoadContent()
            .addContentLoader(() => StaticPictureLoader.loadContent());

        this.content = contentLoader.loadContent();
    }

    render() {
        var $galleryEle = $(this.elementSelector);
        var nodes = this.content.map(content => content.renderGallery());

        $galleryEle.html(
            '<header class="app-header">' +
                '<h1 class="title">Gallery</h1>' +
            '</header>' +
            '<main class="screen-width-limit">' +
                '<ul class="gallery-items">' +
                    nodes.join('') +
                '</ul>' +
            '</main>'
        );
    }

    // Gallery Item sorting, filtering and searching to be done based on content data
    sort() {

        // Example of sorting by name, other sorting would happen in similar ways
        return this.content.sort((itemA, itemB) => {
            var nameA = itemA.name.toUpperCase();
            var nameB = itemB.name.toUpperCase();

            if (nameA < nameB) {
                return -1;
            }
            else if (nameA > nameB) {
                return 1;
            }

            return 0;
        });
    }

    // Filtering and sorting to call through to 'LoadContent' class 
    // to make use of DB searching / filtering for datasets
    filter() { }

    search() { }

    // Function that highlights and scrolls to a viewed item when returning to the gallery listing page
    static highlightViewedItem(id) {
        $('.gallery-items .item').removeClass('viewed');
        var $viewedItem = $('.gallery-items .item.' + id);
        if ($viewedItem[0]) {
            $viewedItem[0].scrollIntoView({ behavior: 'smooth' });
        }
        $viewedItem.addClass('viewed');
    }
}