// Class for registering toolbar actions, only accepts actions that extend the base action interface
class ActionsRegister {
    constructor(fileActions) {
        this.fileActions = fileActions && fileActions.filter(fileAction => fileAction instanceof BaseAction);

        if(!this.fileActions) {
            this.fileActions = [];
        }
    }

    registerAction(fileAction) {
        if (fileAction instanceof BaseAction) {
            this.fileActions.push(fileAction);
        }
        else {
            throw new Error('Invalid file action type');
        }

        return this;
    }

    renderActions() {
        var actionNodes = [];

        this.fileActions.forEach((fileAction, i) => {

            $('body').on('click', '#' + fileAction.actionSelectorId + ' button',
                fileAction.contentAction.bind(fileAction));

            actionNodes.push(
                Mustache.render(fileAction.renderAction(), fileAction)
            );
        });

        return actionNodes;
    }
}