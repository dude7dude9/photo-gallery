// Interface for registering actions that can be performed within the item viewer
// Ensure that additional functions integrate correctly
// Makes it easier for external parties to integrate/add functionality
class BaseAction {
    constructor(iconUrl, actionSelectorId) {
        this.icon = iconUrl;
        this.actionSelectorId = actionSelectorId;

        // Ensure that required interface variables are supplied
        if (!this.icon || !this.actionSelectorId) {
            throw new Error('Action constructor arguments missing');
        }
    }

    // Ensure that the interface methods are implemented
    contentAction() {
        throw new Error('Content Action not implemented');
    }
}