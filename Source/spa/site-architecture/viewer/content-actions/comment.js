class Comment extends BaseAction {
    constructor(iconUrl, actionSelectorId) {
        super(iconUrl, actionSelectorId);
    }

    contentAction() {
        //Demonstrating that class scoping etc is preserved and not just function parsed into click function
        this.anotherAction();
    }

    anotherAction() {
        alert('Post Comment');
    }

    renderAction() {
        var actionTemplate = 
            '<li id="{{actionSelectorId}}">' +
                '<button type="button"><span class="fas fa-comment-alt"></span></button>' +
            '</li>';

        return actionTemplate;
    }
}