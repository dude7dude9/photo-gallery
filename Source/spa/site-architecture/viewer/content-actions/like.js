class Like extends BaseAction {
    constructor(iconUrl, actionSelectorId) {
        super(iconUrl, actionSelectorId);
    }

    contentAction() {
        alert('Like Item');
    }

    renderAction() {
        var actionTemplate = 
            '<li id="{{actionSelectorId}}">' +
                '<button type="button"><span class="fas fa-thumbs-up"></span></button>' +
            '</li>';

        return actionTemplate;
    }
}