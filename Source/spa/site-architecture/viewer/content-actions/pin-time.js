class PinTime extends BaseAction {
    constructor(iconUrl, actionSelectorId) {
        super(iconUrl, actionSelectorId);
    }

    contentAction() {
        alert('Add Item Tags');
    }

    renderAction() {
        var actionTemplate = 
            '<li id="{{actionSelectorId}}">' +
                '<button type="button"><span class="fas fa-stopwatch"></span></button>' +
            '</li>';

        return actionTemplate;
    }
}