class Share extends BaseAction {
    constructor(iconUrl, actionSelectorId) {
        super(iconUrl, actionSelectorId);
    }

    contentAction() {
        alert('Share Item');
    }

    renderAction() {
        var actionTemplate = 
            '<li id="{{actionSelectorId}}">' +
                '<button type="button"><span class="fas fa-share"></span></button>' +
            '</li>';

        return actionTemplate;
    }
}