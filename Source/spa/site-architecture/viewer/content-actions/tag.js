class Tag extends BaseAction {
    constructor(iconUrl, actionSelectorId) {
        super(iconUrl, actionSelectorId);
    }

    contentAction() {
        alert('Add a Tag to the image');
    }

    renderAction() {
        var actionTemplate = 
            '<li id="{{actionSelectorId}}">' +
                '<button type="button"><span class="fas fa-tag"></span></button>' +
            '</li>';

        return actionTemplate;
    }
}