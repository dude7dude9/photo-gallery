class Viewer {
    constructor(elementSelector) {
        this.contentItem = {};
        this.elementSelector = elementSelector;
        this.toolbarActions = '';
        this.registeredActions = [];
        this.swipeView = {};
        this.contentNode = '';

        $('body').on('click', 'button.close', (e) => {
            this.close();
        });
    }

    loadFile(id) {
        sessionStorage.setItem('viewedItem', id);

        var contentLoader = new LoadContent()
            .addContentLoader(() => StaticPictureLoader.loadFile(id));

        var contentItem = contentLoader.loadContent()[0];

        if (contentItem instanceof BaseContent) {
            this.contentItem = contentItem;
        }
        else {
            throw new Error('Unrecognized content type');
        }

        this.loadActions();
    }

    // Dynamically change the available toolbar actions based on the viewed file type
    loadActions() {
        this.registeredActions = new ActionsRegister()
            .registerAction(new Like('./dummy-icon-url', 'like-action'))
            .registerAction(new Comment('./dummy-icon-url', 'comment-action'))
            .registerAction(new Share('./dummy-icon-url', 'share-action'));


        
        // This example demonstrates allowing tagging of people/this for an image
        // Where the video example is to add pins for points of interest in the video timeline
        if (this.contentItem instanceof Picture) {
            this.registeredActions.registerAction(new Tag('./dummy-icon-url', 'tag-action'));
        }
        else if (this.contentItem instanceof Video) {
            this.registeredActions.registerAction(new PinTime('./dummy-icon-url', 'pin-time-action'));
        }

        
        this.toolbarActions =
            '<footer>' +
                '<ul class="viewer-toolbar">' +
                    this.registeredActions.renderActions().join('') +
                '</ul>' +
            '</footer>';
    }

    // Closes viewed item and scrolls to the viewed in the gallery listing
    close() {
        var id = window.location.pathname.split('/')[1].split('id:')[1];

        $('.swipe-wrap').addClass('shrink');
        history.pushState(id, null, '/');
        Routing.loadAppPage('/');
        Gallery.highlightViewedItem(id);
    }

    render() {
        if (this.contentItem.image) {
            this.contentItem.image.onload = this.renderSwipe.bind(this);
        }
        
        this.contentNode = this.contentItem.renderViewer();
    }

    // Mobile swipe for viewing next/previous photo
    renderSwipe() {
        $(this.elementSelector).html(this.contentNode + this.toolbarActions);
        window.scrollTo(0, 0);

        var currentItemId = parseInt(this.contentItem.id.split('-')[1]);
        var startSlide = (currentItemId > 1) ? 1 : 0;

        this.swipeView = new Swipe($('main.slider')[0],
            {
                continuous: false,
                startSlide: startSlide,
                transitionEnd: (i, ele) => {
                    var id = $(ele).data().id;

                    if (id !== window.location.pathname.split('id:')[1]) {
                        history.pushState(id, null, '/id:' + id);
                        Routing.loadAppPage('/id:' + id);
                    }
                }
            });

        // After slider loads to stop flickering when rolling between images
        $(this.elementSelector).find('.content')
            .removeClass('hide')
            .removeClass('show');
    }
}